//
//  main.m
//  YSpeak
//
//  Created by Zack McBride on 25/10/2013.
//  Copyright (c) 2013 Zack McBride. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YSAppDelegate class]));
    }
}
