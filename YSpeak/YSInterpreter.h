//
//  YSInterpreter.h
//  YSpeak
//
//  Created by Zack McBride on 25/10/2013.
//  Copyright (c) 2013 Zack McBride. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark InterpreterViewDelegate protocol

// Here we set up the delegate that allows the
// asynchronous HTTP request to update the text
// on the UI. This delegate and the protocol
// below it can be accessed from anywhere in
// the app, allowing for quickly scaling to
// multiple different interpreters.

@protocol InterpreterViewDelegate <NSObject>

@required
- (void)updateText:(NSString*)output error:(NSError*)error;

@end



#pragma mark InterpreterProtocol protocol

// This sets up the protocol for an individual
// interpreter. This is cleaner than using
// subclasses and allows for multiple different
// APIs and translations to be hooked up with
// minimal replicated code.

@protocol InterpreterProtocol <NSObject>

@required
- (id)interpretText:(NSString*)input withDelegate:(id<InterpreterViewDelegate>)adelegate withError:(NSError **)error;

@end


@interface YSInterpreter : NSObject

@end
